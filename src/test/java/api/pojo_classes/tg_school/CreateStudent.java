package api.pojo_classes.tg_school;

import groovyjarjarantlr4.v4.codegen.model.SrcOp;
import lombok.Builder;
import lombok.Data;

@Data
@Builder

public class CreateStudent {
    private String firstName;
    private String lastName;
    private String email;
    private String dob;


}
