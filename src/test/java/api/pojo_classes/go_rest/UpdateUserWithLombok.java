package api.pojo_classes.go_rest;
import lombok.Data;
import lombok.Builder;

@Data

@Builder
public class UpdateUserWithLombok {


    private String name;
    private String email;


}
